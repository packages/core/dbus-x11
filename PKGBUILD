# Maintainer: Philip Müller <philm@manjaro.org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>
# Contributor: Jan de Groot <jgc@archlinux.org>
# Contributor: Tom Gundersen <teg@jklm.no>
# Contributor: Link Dupont <link@subpop.net>

pkgname=dbus-x11
pkgver=1.14.4
pkgrel=1
pkgdesc="Freedesktop.org message bus system"
url="https://wiki.freedesktop.org/www/Software/dbus/"
arch=(x86_64)
license=(GPL custom)
depends=(systemd-libs expat audit libsystemd.so libaudit.so)
makedepends=(systemd xmlto docbook-xsl python yelp-tools doxygen git autoconf-archive libx11)
provides=('libdbus' 'libdbus-1.so' "dbus=$pkgver")
conflicts=('libdbus' 'dbus')
source=("git+https://gitlab.freedesktop.org/dbus/dbus.git?signed#tag=dbus-$pkgver"
        dbus-reload.hook no-fatal-warnings.diff)
sha256sums=('SKIP'
            'd636205622d0ee3b0734360225739ef0c7ad2468a09489e6ef773d88252960f3'
            'c10395be67e1127a58d7173b587fbbf16f8a8b271c41293558fcf9e27c185478')
validpgpkeys=('DA98F25C0871C49A59EAFF2C4DE8FF2A63C7CC90') # Simon McVittie <simon.mcvittie@collabora.co.uk>

prepare() {
  cd dbus

  # Allow us to enable checks without them being fatal
  patch -Np1 -i ../no-fatal-warnings.diff

  NOCONFIGURE=1 ./autogen.sh
}

build() {
  cd dbus
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --libexecdir=/usr/lib/dbus-1.0 \
    --runstatedir=/run \
    --with-console-auth-dir=/run/console/ \
    --with-dbus-user=dbus \
    --with-system-pid-file=/run/dbus/pid \
    --with-system-socket=/run/dbus/system_bus_socket \
    --with-systemdsystemunitdir=/usr/lib/systemd/system \
    --enable-inotify \
    --enable-libaudit \
    --enable-systemd \
    --enable-user-session \
    --enable-x11-autolaunch \
    --disable-static \
    --disable-verbose-mode \
    --disable-asserts \
    --disable-checks
  make
}

check() {
  make -C dbus -j1 check
}

package() {
  DESTDIR="$pkgdir" make -C dbus install

  rm -r "$pkgdir"/{etc,var}

  # We have a pre-assigned uid (81)
  echo 'u dbus 81 "System Message Bus"' |
    install -Dm644 /dev/stdin "$pkgdir/usr/lib/sysusers.d/dbus.conf"

  install -Dt "$pkgdir/usr/share/libalpm/hooks" -m644 *.hook
  install -Dt "$pkgdir/usr/share/licenses/$pkgname" -m644 dbus/COPYING

  # Remove docs, provided by dbus-docs
  rm -rf "$pkgdir/usr/share/doc/"
}

# vim:set sw=2 et:
